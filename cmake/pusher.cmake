###############################################################################
#
# pusher.nodeenv:   pusher.cmake
#
###############################################################################
#
# This file creates macros and sets defaults for the "Pusher Node JS 
# environment" -- in other words, this is not a generic library, but rather 
# enforces some settings standard to a typical Pusher project.  
#
# @todo Split into generic sub-files and a master file
#


# Will force MSVC, etc. to be determined (otherwise they are not determined until
# PROJECT() is called).
ENABLE_LANGUAGE( CXX )

#
# Use the static runtime library for compatibility with Node JS
#
IF( MSVC )
    STRING( REPLACE /MDd /MTd CMAKE_C_FLAGS_DEBUG   ${CMAKE_C_FLAGS_DEBUG} )
    STRING( REPLACE /MDd /MTd CMAKE_CXX_FLAGS_DEBUG ${CMAKE_CXX_FLAGS_DEBUG} )
    STRING( REPLACE /MD /MT CMAKE_C_FLAGS_RELEASE   ${CMAKE_C_FLAGS_RELEASE} )
    STRING( REPLACE /MD /MT CMAKE_CXX_FLAGS_RELEASE ${CMAKE_CXX_FLAGS_RELEASE} )
ENDIF()



#
# Make sure C++11 is enabled on GCC
# 
IF( CMAKE_COMPILER_IS_GNUCXX)
    SET( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11" )
ENDIF()


# -----------------------------------------------------------------------------
# GLOB_SOURCES_VAR
# -----------------------------------------------------------------------------
# Recursively searches the BASE directory for *.c, *.cpp, *.h, *.hpp and
# set matching files to the SOURCES variable. Automatically puts any 
# found files in a Visual Studio source group under 
# "Source/<path relative to BASE>".
#
MACRO( PN_GLOB_SOURCES_VAR )
    
	SET( SOURCES )
	SET( BASESET ${ARGV}  )
	FOREACH( BASE ${BASESET} )
		# Glob the files relative to the base, so the sub-path is easier to extract
		FILE( GLOB_RECURSE SRC_C RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/${BASE} ${BASE}/*.c)
		FILE( GLOB_RECURSE SRC_H RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/${BASE} ${BASE}/*.h)
		FILE( GLOB_RECURSE SRC_CPP RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/${BASE} ${BASE}/*.cpp)
		FILE( GLOB_RECURSE SRC_HPP RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/${BASE} ${BASE}/*.hpp)
		SET( SRC_ALL ${SRC_C} ${SRC_CPP} ${SRC_H} ${SRC_HPP} )
     
		FOREACH ( F ${SRC_ALL} )      
			# Put the file in the appropriate source group
			GET_FILENAME_COMPONENT( DIR ${F} PATH )
			STRING( REPLACE "/" "\\" DIR "${DIR}" )
			SOURCE_GROUP( "Source\\${BASE}\\${DIR}" FILES ${BASE}/${F})
    
			# Append to the SOURCES variable
			SET( SOURCES ${SOURCES} "${BASE}/${F}")
		ENDFOREACH (F)
	ENDFOREACH (BASE)

ENDMACRO( PN_GLOB_SOURCES_VAR BASE )

# -----------------------------------------------------------------------------

