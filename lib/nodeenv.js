/* ----------------------------------------------------------------------------
    pusher.flatdb
    MIT License, Copyright (c) 2013 Pusher, Inc.
   -------------------------------------------------------------------------- */
/*
    Permission is hereby granted, free of charge, to any person obtaining a 
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation 
    the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in 
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------------
*/

var fs = require("fs")
    _  = require("underscore");

var nodeunit =
{
    suite :
    {
        "NPM Package Conventions" :
        {
            /*!
                Enforce a few conventions.
             */
            "package.json" : function (test)
            {            
                var package = undefined;
                
                test.doesNotThrow(function() {
                    var text = fs.readFileSync("package.json", "utf8");
                    package = JSON.parse(text);            
                });
                
                // Don't bother with these if the parse failed...
                if (typeof package !== "undefined")
                {
                    test.equal(typeof package.name, "string", "package name not specified");
                    test.notEqual(package.name, "", "package name not specified");
                    
                    test.equal(typeof package.author, "string", "package author not specified");
                    test.notEqual(package.author, "", "package author not specified");
                    
                    test.equal(typeof package.description, "string", "package description not specified");
                    test.notEqual(package.description, "", "package description not specified");
                    
                    test.equal(typeof package.version, "string", "package version not specified");
                    test.notEqual(package.version, "", "package version not specified");
                    
                    test.equal(typeof package.license, "string", "License defined in package.json");
                    test.notEqual(package.license, "");
                }
                test.done();        
            },
            
            "component.json" : function (test)
            {
                if ( fs.existsSync("component.json") )
                {
                    var package = {};
                    test.doesNotThrow(function() {
                        package = JSON.parse( fs.readFileSync("component.json", "utf8") );            
                    });
                }
                test.done();        
            },

            "bower / NPM equivalence" : function (test)
            {
                if ( fs.existsSync("component.json") )
                {
                    var comp = JSON.parse( require("fs").readFileSync("component.json", "utf8") );
                    var pack = JSON.parse( require("fs").readFileSync("package.json", "utf8") );
                    
                    test.equal( pack.name ,   comp.name );
                    test.equal( pack.version, comp.version );
                    test.equal( pack.main,    comp.main );
                }
                test.done();
            },
            
            ".gitignore" : function (test)
            {
                var entries = {};
                test.doesNotThrow(function() {
                    var content = require("fs").readFileSync(".gitignore", "utf8");
                    _.each( content.split(/\n/g), function (line) { 
                        line = line.replace(/^\s+/, "").replace(/\s+$/, "");
                        if (line.length > 0)
                            entries[line] = true;
                    });            
                });
                
                var expectedEntries =
                [
                    "node_modules",
                    "npm-debug.log"
                ];
                _.each(expectedEntries, function (entry) {
                    test.ok(entries[entry], ".gitignore does not contain " + entry);
                });
                
                test.done();
            },
            
            "expected files" : function (test)
            {
                var fs = require("fs");
                test.ok( fs.existsSync(".gitattributes"), ".gitattributes file not found" );
                test.ok( fs.existsSync("README.md") );
                test.ok( fs.existsSync("benchmark"), "'benchmark' directory does not exist" );
                test.ok( fs.existsSync("test") );
            
                test.done();
            },
            
        }
    }
};

exports.nodeunit = nodeunit;
