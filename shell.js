try
{
    require("shelljs/global");
}
catch (e)
{
    console.log("Error: Could not load required node modules");
    console.log("Have you run `npm install --dev`?");
    process.exit(-1);
}


var lib = {};
lib._win32 = (require("os").platform() == "win32");
    
//
// Quick workaround for https://github.com/arturadib/shelljs/issues/41
//  
lib.runcmd = exec;
if (require("os").platform() == "win32") 
{
    lib.runcmd = function() 
    { 
        arguments[0] = arguments[0].replace(/\//g, "\\");
        exec.apply(null, arguments);
    }
}

lib.runbuilt = function(name)
{
    var path = lib._win32 
        ? ".\\build\\Release\\" + name + ".exe"
        : "./build/" + name;
    return lib.runcmd(path);
}


//
// This is a scripting convenience library: put all the utilities in 
// global namespace.
//
for (name in lib)
    global[name] = lib[name];
